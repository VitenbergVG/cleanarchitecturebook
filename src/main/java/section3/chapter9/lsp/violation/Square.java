package section3.chapter9.lsp.violation;

public class Square extends Rectangle {

    public void setWidth(int width) {
        setSide(width);
    }

    public void setHeight(int height) {
        setSide(height);
    }

    public void setSide(int side) {
        super.setWidth(side);
        super.setHeight(side);
    }
}
