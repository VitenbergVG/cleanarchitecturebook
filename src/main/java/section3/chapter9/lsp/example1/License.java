package section3.chapter9.lsp.example1;

public interface License {

    double calcFee();
}
