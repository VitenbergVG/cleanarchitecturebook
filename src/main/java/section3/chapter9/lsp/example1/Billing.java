package section3.chapter9.lsp.example1;

public class Billing {

    License license;

    public Billing(License license) {
        this.license = license;
    }

    /**
     * Этот дизайн соответствует принципу подстановки Барбары Лисков, потому
     * что поведение класса Billing не зависит от использования того или
     * иного подтипа. Оба подтипа могут служить заменой для типа License
     */
    public void bill() {
        double v = license.calcFee();
        System.out.println(v);
    }
}
