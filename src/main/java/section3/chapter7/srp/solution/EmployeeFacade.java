package section3.chapter7.srp.solution;

public class EmployeeFacade {

    private final PayCalculator payCalculator;
    private final HourReporter hourReporter;
    private final EmployeeSaver employeeSaver;

    public EmployeeFacade(PayCalculator payCalculator, HourReporter hourReporter, EmployeeSaver employeeSaver) {
        this.payCalculator = payCalculator;
        this.hourReporter = hourReporter;
        this.employeeSaver = employeeSaver;
    }

    /**
     * Метод для расчета зарплаты. Определяется бухгалтерией.
     */
    public double calculatePay() {
        return payCalculator.calculatePay();
    }

    /**
     * Метод для отчета о часах. Определяется и используется отделом по работе с персоналом.
     */
    public int reportHours() {
        return hourReporter.reportHours();
    }

    /**
     * Метод для сохранения данных о сотруднике. Определяется администратором базы данных.
     */
    public void save() {
        employeeSaver.save();
    }
}
