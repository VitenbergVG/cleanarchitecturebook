package section3.chapter7.srp.violation;

/**
 * Класс с непреднамеренным дублированием, нарушение принципов SRP.
 * Методы имеют разных акторов.
 */
public class Employee {

    /**
     * Метод для расчета зарплаты. Определяется бухгалтерией.
     */
    public double calculatePay() {
        int regularHours = regularHours();
        // Calculate Pay
        return 0.0;
    }

    /**
     * Метод для отчета о часах. Определяется и используется отделом по работе с персоналом.
     */
    public int reportHours() {
        int regularHours = regularHours();
        // Report hours
        return 0;
    }

    /**
     * Метод для сохранения данных о сотруднике. Определяется администратором базы данных.
     */
    public void save() {
        // Save employee
    }

    /**
     * Общий метод для расчета часов работы.
     */
    private int regularHours() {
        // Calculate regular hours
        return 0;
    }
}
