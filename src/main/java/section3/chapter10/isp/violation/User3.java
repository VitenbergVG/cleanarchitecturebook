package section3.chapter10.isp.violation;

public class User3 implements Operations {

    /**
     * Использует только 1 из методов интерфейса operation3.
     */
    public void useOperation() {
        operation3();
    }

    @Override
    public void operation1() {
        // empty because it is not used
    }

    @Override
    public void operation2() {
        // empty because it is not used
    }

    @Override
    public void operation3() {
        // some logic
    }
}
