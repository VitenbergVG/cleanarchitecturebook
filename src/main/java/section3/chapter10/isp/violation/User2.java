package section3.chapter10.isp.violation;

public class User2 implements Operations {

    /**
     * Использует только 1 из методов интерфейса operation2.
     */
    public void useOperation() {
        operation2();
    }


    @Override
    public void operation1() {
        // empty because it is not used
    }

    @Override
    public void operation2() {
        // some logic
    }

    @Override
    public void operation3() {
        // empty because it is not used
    }
}
