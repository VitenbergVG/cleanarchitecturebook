package section3.chapter10.isp.violation;

public interface Operations {

    void operation1();
    void operation2();
    void operation3();
}
