package section3.chapter10.isp.violation;

public class User1 implements Operations {

    /**
     * Использует только 1 из методов интерфейса operation1.
     */
    public void useOperation() {
        operation1();
    }

    @Override
    public void operation1() {
        // some logic
    }

    @Override
    public void operation2() {
        // empty because it is not used
    }

    @Override
    public void operation3() {
        // empty because it is not used
    }
}
