package section3.chapter10.isp.solution;

public class User2 implements User2Operations {

    /**
     * Использует только 1 из методов интерфейса operation2.
     */
    public void useOperation() {
        operation2();
    }

    @Override
    public void operation2() {
        // some logic
    }
}
