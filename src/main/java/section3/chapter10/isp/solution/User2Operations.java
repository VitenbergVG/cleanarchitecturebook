package section3.chapter10.isp.solution;

public interface User2Operations extends Operations {

    @Override
    default void operation1() {
        // empty because it is not used
    }

    @Override
    default void operation3() {
        // empty because it is not used
    }
}
