package section3.chapter10.isp.solution;

public interface User1Operations extends Operations {

    @Override
    default void operation2() {
        // empty because it is not used
    }

    @Override
    default void operation3() {
        // empty because it is not used
    }
}
