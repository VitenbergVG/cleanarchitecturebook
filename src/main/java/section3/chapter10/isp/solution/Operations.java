package section3.chapter10.isp.solution;

public interface Operations {

    void operation1();
    void operation2();
    void operation3();
}
