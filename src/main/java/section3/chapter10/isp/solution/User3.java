package section3.chapter10.isp.solution;

public class User3 implements User3Operations {

    /**
     * Использует только 1 из методов интерфейса operation3.
     */
    public void useOperation() {
        operation3();
    }

    @Override
    public void operation3() {
        // some logic
    }
}
