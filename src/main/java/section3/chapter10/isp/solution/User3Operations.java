package section3.chapter10.isp.solution;

public interface User3Operations extends Operations {

    @Override
    default void operation1() {
        // empty because it is not used
    }

    @Override
    default void operation2() {
        // empty because it is not used
    }
}
