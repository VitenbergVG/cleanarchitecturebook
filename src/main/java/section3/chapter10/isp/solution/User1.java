package section3.chapter10.isp.solution;

public class User1 implements User1Operations {

    /**
     * Использует только 1 из методов интерфейса operation1.
     */
    public void useOperation() {
        operation1();
    }

    @Override
    public void operation1() {
        // some logic
    }
}
