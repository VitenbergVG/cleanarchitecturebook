package section3.chapter11.dip.solution.abstractfactory;

public class ServiceFactoryImpl implements ServiceFactory {

    @Override
    public Service makeService() {
        return new ConcreteServiceImpl();
    }
}
