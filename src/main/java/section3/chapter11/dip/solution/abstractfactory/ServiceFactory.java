package section3.chapter11.dip.solution.abstractfactory;

public interface ServiceFactory {

    Service makeService();
}
