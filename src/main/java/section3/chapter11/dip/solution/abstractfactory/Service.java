package section3.chapter11.dip.solution.abstractfactory;

public interface Service {

    void doWork();
}
