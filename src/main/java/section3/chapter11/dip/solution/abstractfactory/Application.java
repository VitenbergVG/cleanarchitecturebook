package section3.chapter11.dip.solution.abstractfactory;

public class Application {

    private final ServiceFactory serviceFactory;

    public Application(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    public void doServiceWork() {
        Service service = serviceFactory.makeService();
        service.doWork();
    }
}
